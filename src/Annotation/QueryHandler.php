<?php

namespace Drupal\command_query_separation\Annotation;

/**
 *
 * @Annotation
 */
class QueryHandler extends HandlerAnnotation  {
  public $query_id;
}