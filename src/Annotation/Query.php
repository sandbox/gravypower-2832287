<?php

namespace Drupal\command_query_separation\Annotation;


use Drupal\Component\Annotation\AnnotationInterface;
use Drupal\Component\Annotation\Plugin;

/**
 * @Annotation
 */
class Query extends Plugin implements AnnotationInterface{
  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin names.
   *
   * @var string
   */
  public $name;
}