<?php
/**
 * Created by PhpStorm.
 * User: aaron
 * Date: 12/2/16
 * Time: 12:10 PM
 */

namespace Drupal\command_query_separation\Annotation;

/**
 * @Annotation
 */
class CommandHandler extends HandlerAnnotation {
  public $command_id;
}