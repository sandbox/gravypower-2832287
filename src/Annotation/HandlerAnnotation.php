<?php

namespace Drupal\command_query_separation\Annotation;


use Drupal\Component\Annotation\AnnotationInterface;
use Drupal\Component\Annotation\Plugin;

abstract class HandlerAnnotation extends Plugin  implements AnnotationInterface  {
  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the form plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

  /**
   * The handler to decorate
   *
   * @var string
   */
  public $decorated_handler_id;
}