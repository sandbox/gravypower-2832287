<?php

namespace Drupal\command_query_separation\Annotation;


use Drupal\Component\Annotation\AnnotationInterface;
use Drupal\Component\Annotation\Plugin;

/**
 * @Annotation
 */
class Command  extends Plugin  implements AnnotationInterface{
  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin name.
   *
   * @var string
   */
  public $name;

}