<?php

namespace Drupal\command_query_separation;

use Drupal\Core\Plugin\ContextAwarePluginBase;

abstract class Handler extends ContextAwarePluginBase implements IHandler {
  /**
   * @var IHandler
   */
  protected $decorated_handler;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->pluginDefinition['name'];
  }

  public function getTypeId() {
    if (array_key_exists('query_id', $this->pluginDefinition)) {
      return $this->pluginDefinition['query_id'];
    }
    elseif (array_key_exists('command_id', $this->pluginDefinition)) {
      return $this->pluginDefinition['command_id'];
    }

    return NULL;
  }

  public function getDecoratedHandlerId() {
    if (array_key_exists('decorated_handler_id', $this->pluginDefinition)) {
      return $this->pluginDefinition['decorated_handler_id'];
    }
    return NULL;
  }

  /**
   * @param $decorated_handler
   * @return mixed|void
   */
  function setDecoratedHandler($decorated_handler) {
    $this->decorated_handler = $decorated_handler;
  }

  /**
   * @param $object
   * @return mixed
   */
  abstract function handle($object);


}