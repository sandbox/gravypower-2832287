<?php

namespace Drupal\command_query_separation;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;

interface IHandler extends ContextAwarePluginInterface, CacheableDependencyInterface {

  /**
   *
   * @return string
   */
  public function getName();


  /**
   *
   * @return string
   */
  public function getDecoratedHandlerId();

  /**
   * @param $decorated_handler
   * @return mixed
   */
  public function setDecoratedHandler($decorated_handler);

  /**
   * @param $object
   * @return mixed
   */
  public function handle($object);
}