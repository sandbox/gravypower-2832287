<?php

namespace Drupal\command_query_separation\PluginManagers;

use Drupal\command_query_separation\Exceptions\MultipleHandlersFound;
use Drupal\command_query_separation\Exceptions\NoHandlerFound;
use Drupal\command_query_separation\Handler;
use Drupal\command_query_separation\IHandler;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Theme\ThemeManagerInterface;
use ReflectionClass;
use Traversable;

abstract class HandlerManager extends Manager {

  /**
   * @var \Drupal\Core\Plugin\DefaultPluginManager
   */
  private $commandQueryManager;

  /**
   * HandlerManager constructor.
   * @param bool|string $subdir
   * @param \Traversable $namespaces
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param string $plugin_definition_annotation_name
   * @param array|\string[] $additional_annotation_namespaces
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $handler_id
   * @param \Drupal\Core\Plugin\DefaultPluginManager $commandQueryManager
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   * @param \Drupal\command_query_separation\PluginManagers\ThemeHandlerInterface $themeHandler
   */
  public function __construct(
    $subdir,
    Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    $plugin_definition_annotation_name,
    $additional_annotation_namespaces,
    $handler_id,
    DefaultPluginManager $commandQueryManager,
    ThemeManagerInterface $themeManager,
    ThemeHandlerInterface $themeHandler) {

    parent::__construct(
      $subdir,
      $namespaces,
      $module_handler,
      $plugin_definition_annotation_name,
      $additional_annotation_namespaces,
      $themeManager,
      $themeHandler
    );

    $this->alterInfo($handler_id . '_info');
    $this->setCacheBackend($cache_backend, $handler_id);
    $this->commandQueryManager = $commandQueryManager;

  }

  /**
   * @param $commandQuery
   * @return mixed
   */
  protected function doHandle($commandQuery) {

    $commandQueryId = $this->resolvePluginId($commandQuery);
    $commandQuery = $this->resolveType($commandQuery);
    $handler = $this->findHandler($commandQueryId);
    $decorators = $this->findDecorators($handler);
    $handler = $this->decorateInstance($decorators, $handler);

    return $handler->handle($commandQuery);
  }

  /**
   * @param $commandQuery
   * @param $instances
   * @throws \Drupal\command_query_separation\Exceptions\MultipleHandlersFound
   * @throws \Drupal\command_query_separation\Exceptions\NoHandlerFound
   */
  protected function GuardHandlers($commandQuery, $instances) {
    $instanceCount = count($instances);

    if ($instanceCount == 0) {
      throw new NoHandlerFound($commandQuery);
    }
    elseif ($instanceCount > 1) {
      throw new MultipleHandlersFound($commandQuery, $instances);
    }
  }

  /**
   * @param Handler $instance
   * @return array
   */
  protected function findDecorators($instance) {
    $decorators = array();
    foreach ($this->getDefinitions() as $handler) {
      if (array_key_exists('decorated_handler_id', $handler)) {
        if ($handler['decorated_handler_id'] == $instance->getPluginId()) {
          /** @var Handler $decorator */
          $decorator = $this->createInstance($handler['id']);
          $decorator->setDecoratedHandler($instance);

          $decorators[$decorator->getPluginId()] = $decorator;
        }
      }
    }

    return array_reverse($decorators);
  }

  /**
   * @param $commandQuery
   * @return \Drupal\command_query_separation\Handler
   */
  protected function findHandler($commandQuery) {
    $instances = array();

    foreach ($this->getDefinitions() as $handler) {
      /** @var Handler $instance */
      $instance = $this->createInstance($handler['id']);

      if ($instance->getTypeId() == $commandQuery) {
        /** @var IHandler $instance */
        $instances[$instance->getPluginId()] = $instance;
      }
    }

    $this->GuardHandlers($commandQuery, $instances);

    $instance = reset($instances);
    return $instance;
  }

  /**
   * @param $decorators
   * @param $instance
   * @return mixed
   */
  protected function decorateInstance($decorators, $instance) {
    /** @var Handler $decorator */
    foreach ($decorators as $decorator) {
      $decorator->setDecoratedHandler($instance);
      $instance = $decorator;
    }
    return $instance;
  }

  /**
   * @param $commandQuery
   * @return \ReflectionClass
   */
  protected function resolveType($commandQuery) {
    if (gettype($commandQuery) == 'string') {
      foreach ($this->commandQueryManager->getDefinitions() as $commandQueryPlugin) {
        if ($commandQueryPlugin['id'] == $commandQuery) {
          $commandQuery = $this->commandQueryManager->createInstance($commandQueryPlugin['id']);
          break;
        }
      }
      return $commandQuery;
    }
    return $commandQuery;
  }

  /**
   * @param $commandQuery
   * @return \ReflectionClass
   */
  protected function resolvePluginId($commandQuery) {
    $commandQueryId = $commandQuery;
    if (gettype($commandQuery) != 'string') {
      /** @var ReflectionClass $commandQuery */
      $reflectionClass = new ReflectionClass($commandQuery);

      foreach ($this->commandQueryManager->getDefinitions() as $commandQueryPlugin) {
        if ($commandQueryPlugin['class'] == $reflectionClass->getName()) {
          $commandQueryId = $commandQueryPlugin['id'];
          break;
        }
      }
      return $commandQueryId;
    }
    return $commandQueryId;
  }
}