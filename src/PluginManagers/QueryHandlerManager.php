<?php

namespace Drupal\command_query_separation\PluginManagers;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Traversable;

class QueryHandlerManager extends HandlerManager {

  public function __construct(Traversable $namespaces,
                              CacheBackendInterface $cache_backend,
                              ModuleHandlerInterface $module_handler,
                              QueryManager $query_manager,
                              ThemeManagerInterface $themeManager,
                              ThemeHandlerInterface $themeHandler) {
    parent::__construct(
      'Plugin/CommandQuerySeparation/QueryHandlers',
      $namespaces,
      $cache_backend,
      $module_handler,
      'Drupal\command_query_separation\IQueryHandler',
      'Drupal\command_query_separation\Annotation\QueryHandler',
      'query_handler',
      $query_manager,
      $themeManager,
      $themeHandler
    );
  }

  public function handle($query)
  {
    return $this->doHandle($query);
  }
}