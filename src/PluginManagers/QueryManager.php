<?php

namespace Drupal\command_query_separation\PluginManagers;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Traversable;

class QueryManager extends Manager {
  /**
   * Constructs an RestEventFilterPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations,
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   */
  public function __construct(Traversable $namespaces,
                              CacheBackendInterface $cache_backend,
                              ModuleHandlerInterface $module_handler,
                              ThemeManagerInterface $themeManager,
                              ThemeHandlerInterface $themeHandler) {
    parent::__construct(
      'Plugin/CommandQuerySeparation/Queries',
      $namespaces,
      $module_handler,
      'Drupal\command_query_separation\IQuery',
      'Drupal\command_query_separation\Annotation\Query',
      $themeManager,
      $themeHandler);

    $this->alterInfo('queries_info');
    $this->setCacheBackend($cache_backend, 'queries');
  }

}