<?php

namespace Drupal\command_query_separation\PluginManagers;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Traversable;

class CommandHandlerManager extends HandlerManager {

  public function __construct(Traversable $namespaces,
                              CacheBackendInterface $cache_backend,
                              ModuleHandlerInterface $module_handler,
                              CommandManager $command_manager,
                              ThemeManagerInterface $themeManager,
                              ThemeHandlerInterface $themeHandler) {
    parent::__construct(
      'Plugin/CommandQuerySeparation/CommandHandlers',
      $namespaces,
      $cache_backend,
      $module_handler,
      'Drupal\command_query_separation\ICommandHandler',
      'Drupal\command_query_separation\Annotation\CommandHandler',
      'command_handler',
      $command_manager,
      $themeManager,
      $themeHandler
    );

  }
  public function handle($command)
  {
    return $this->doHandle($command);
  }

}