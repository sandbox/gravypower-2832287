<?php


namespace Drupal\command_query_separation\PluginManagers;


use ArrayObject;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Theme\ActiveTheme;
use Drupal\Core\Theme\ThemeManagerInterface;
use Traversable;

abstract class Manager extends DefaultPluginManager{

  /**
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  private $themeHandler;

  public function __construct(
    $subdir,
    Traversable $namespaces,
    ModuleHandlerInterface $module_handler,
    $plugin_definition_annotation_name,
    $additional_annotation_namespaces,
    ThemeManagerInterface $themeManager,
    ThemeHandlerInterface $themeHandler) {

    /** @var ActiveTheme $activeTheme */
    $activeTheme = $themeManager->getActiveTheme();

    $namespacesArray = (array)$namespaces;

    $namespacesArray['Drupal\\' . $activeTheme->getName()] = DRUPAL_ROOT . '/' . $activeTheme->getPath() . '/src';
    foreach ($activeTheme->getBaseThemes() as $ancestor) {
      /** @var ArrayObject $namespaces */
      $namespacesArray['Drupal\\' . $ancestor->getName()] = DRUPAL_ROOT . '/' . $ancestor->getPath() . '/src';
    }

    parent::__construct(
      $subdir,
      new ArrayObject($namespacesArray),
      $module_handler,
      $plugin_definition_annotation_name,
      $additional_annotation_namespaces
    );

    $this->themeHandler = $themeHandler;
  }

  protected function getDiscovery() {
    return parent::getDiscovery();
  }

  protected function providerExists($provider) {
    return $this->themeHandler->themeExists($provider) ||  $this->moduleHandler->moduleExists($provider);
  }
}