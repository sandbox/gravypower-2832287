<?php
namespace Drupal\command_query_separation\Console\Generator;

use Drupal\Console\Core\Utils\TwigRenderer;
use Drupal\Console\Extension\Manager;

use Drupal\Core\Plugin\DefaultPluginManager;

class PluginQueryHandlerGenerator extends HandlerGenerator{

  const twig_template = 'query-handler.php.twig';
  const plugin_path = 'CommandQuerySeparation/QueryHandlers';

  /**
   * PluginFieldFormatterGenerator constructor.
   * @param Manager $extensionManager
   * @param \Drupal\Console\Core\Utils\TwigRenderer $render
   * @param \Drupal\Core\Plugin\DefaultPluginManager $queryManager
   */
  public function __construct(
    Manager $extensionManager,
    TwigRenderer $render,
    DefaultPluginManager $queryManager
  ) {

    parent::__construct($extensionManager, $render, $queryManager, PluginQueryHandlerGenerator::twig_template, PluginQueryHandlerGenerator::plugin_path);
  }
}