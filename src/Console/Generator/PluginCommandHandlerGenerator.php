<?php
namespace Drupal\command_query_separation\Console\Generator;

use Drupal\command_query_separation\PluginManagers\CommandManager;
use Drupal\Console\Core\Utils\TwigRenderer;
use Drupal\Console\Extension\Manager;


class PluginCommandHandlerGenerator extends HandlerGenerator{

  const twig_template = 'command-handler.php.twig';
  const plugin_path = 'CommandQuerySeparation/CommandHandlers';

  /**
   * PluginFieldFormatterGenerator constructor.
   * @param Manager $extensionManager
   * @param \Drupal\Console\Core\Utils\TwigRenderer $render
   * @param \Drupal\command_query_separation\PluginManagers\CommandManager $commandManager
   */
  public function __construct(
    Manager $extensionManager,
    TwigRenderer $render,
    CommandManager $commandManager
  ) {

    parent::__construct($extensionManager, $render, $commandManager, PluginCommandHandlerGenerator::twig_template, PluginCommandHandlerGenerator::plugin_path);
  }
}