<?php

namespace Drupal\command_query_separation\Console\Generator;

use Drupal\Console\Core\Generator\Generator;
use Drupal\Console\Core\Utils\TwigRenderer;
use Drupal\Console\Extension\Manager;

use Drupal\Core\Plugin\DefaultPluginManager;

class HandlerGenerator extends Generator{
  /**
   * @var \Drupal\Console\Extension\Manager
   */
  protected $extensionManager;

  /**
   * @var \Drupal\Core\Plugin\DefaultPluginManager
   */
  protected $manager;


  protected $twig_template;
  /**
   * @var
   */
  private $plugin_path;

  /**
   * PluginFieldFormatterGenerator constructor.
   * @param Manager $extensionManager
   * @param \Drupal\Console\Core\Utils\TwigRenderer $render
   * @param \Drupal\Core\Plugin\DefaultPluginManager $manager
   * @param $twig_template
   * @param $plugin_path
   */
  public function __construct(
    Manager $extensionManager,
    TwigRenderer $render,
    DefaultPluginManager $manager,
    $twig_template,
    $plugin_path
  ) {

    $this->extensionManager = $extensionManager;
    $this->manager = $manager;

    $render->addSkeletonDir($this->extensionManager->getModule('command_query_separation')->getPath() . '/templates/');

    $this->setRenderer($render);
    $this->twig_template = $twig_template;
    $this->plugin_path = $plugin_path;
  }

  /**
   * Generator Plugin Field Formatter.
   *
   * @param string $module Module name
   * @param string $class_name Plugin Class name
   * @param string $label Plugin label
   * @param string $plugin_id Plugin id
   * @param $command_query_id
   */
  public function generate($module, $class_name, $label, $plugin_id, $command_query_id)
  {
    $parameters = [
      'module' => $module,
      'class_name' => $class_name,
      'label' => $label,
      'description' => $label,
      'plugin_id' => $plugin_id,
      'command_query_id' => $command_query_id,
    ];

    $commandDefinition = $this->manager->getDefinition($command_query_id);

    $parameters['command_query_type_fully_qualified_name'] = $commandDefinition['class'];
    $parameters['command_query_type_name'] = end(explode ('\\', $commandDefinition['class'] ));

    $this->renderFile(
      $this->twig_template,
      $this->extensionManager->getPluginPath($module, $this->plugin_path) . '/' . $class_name . '.php',
      $parameters
    );
  }
}