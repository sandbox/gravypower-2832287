<?php

namespace Drupal\command_query_separation\Console\Command;

use Drupal\command_query_separation\Console\Generator\HandlerGenerator;
use Drupal\Console\Command\Shared\ConfirmationTrait;
use Drupal\Console\Command\Shared\ModuleTrait;
use Drupal\Console\Core\Command\Shared\CommandTrait;
use Drupal\Console\Core\Utils\ChainQueue;
use Drupal\Console\Core\Utils\StringConverter;
use Drupal\Console\Extension\Manager;
use Symfony\Component\Console\Command\Command;

class GenerateHandler extends Command {

  use ModuleTrait;
  use ConfirmationTrait;
  use CommandTrait;

  /** @var Manager  */
  protected $extensionManager;

  /**
   * @var ChainQueue
   */
  protected $chainQueue;

  /**
   * @var \Drupal\Console\Core\Utils\StringConverter
   */
  protected $stringConverter;

  /**
   * @var \Drupal\command_query_separation\Console\Generator\HandlerGenerator
   */
  protected $handlerGenerator;

  /**
   * PluginImageFormatterCommand constructor.
   * @param \Drupal\Console\Extension\Manager $extensionManager
   * @param \Drupal\Console\Core\Utils\StringConverter
   * @param ChainQueue $chainQueue
   * @param \Drupal\command_query_separation\Console\Generator\HandlerGenerator $handlerGenerator
   */
  public function __construct(
    Manager $extensionManager,
    StringConverter $stringConverter,
    ChainQueue $chainQueue,
    HandlerGenerator $handlerGenerator
  ) {
    $this->extensionManager = $extensionManager;
    $this->chainQueue = $chainQueue;
    $this->stringConverter = $stringConverter;
    $this->handlerGenerator = $handlerGenerator;

    parent::__construct();
  }
}