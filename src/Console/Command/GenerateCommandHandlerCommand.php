<?php

namespace Drupal\command_query_separation\Console\Command;

use Drupal\command_query_separation\Console\Generator\HandlerGenerator;
use Drupal\command_query_separation\PluginManagers\CommandManager;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\Console\Core\Utils\ChainQueue;
use Drupal\Console\Core\Utils\StringConverter;
use Drupal\Console\Extension\Manager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateCommandHandlerCommand.
 *
 * @package Drupal\command_query_separation
 */
class GenerateCommandHandlerCommand extends GenerateHandler {
  /**
   * @var \Drupal\command_query_separation\PluginManagers\CommandManager
   */
  private $command_manager;

  /**
   * PluginImageFormatterCommand constructor.
   * @param \Drupal\Console\Extension\Manager $extensionManager
   * @param \Drupal\command_query_separation\Console\Generator\HandlerGenerator $handlerGenerator
   * @param \Drupal\Console\Core\Utils\StringConverter $stringConverter
   * @param \Drupal\command_query_separation\PluginManagers\CommandManager $command_manager
   * @param \Drupal\Console\Core\Utils\ChainQueue $chainQueue
   */
  public function __construct(
    Manager $extensionManager,
    HandlerGenerator $handlerGenerator,
    StringConverter $stringConverter,
    CommandManager $command_manager,
    ChainQueue $chainQueue
  ) {

    parent::__construct($extensionManager, $stringConverter, $chainQueue, $handlerGenerator);

    $this->command_manager = $command_manager;

  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('generate:command_query_separation:command_handler')
      ->setDescription($this->trans('commands.generate.command_query_separation.command_handler.description'))
      ->addOption('module', '', InputOption::VALUE_REQUIRED, $this->trans('commands.common.options.module'))
      ->addOption(
        'class',
        '',
        InputOption::VALUE_REQUIRED,
        $this->trans('commands.generate.command_query_separation.command_handler.options.class')
      )
      ->addOption(
        'label',
        '',
        InputOption::VALUE_OPTIONAL,
        $this->trans('commands.generate.command_query_separation.command_handler.options.label')
      )
      ->addOption(
        'plugin-id',
        '',
        InputOption::VALUE_OPTIONAL,
        $this->trans('commands.generate.command_query_separation.command_handler.options.plugin_id')
      )
      ->addOption(
        'command-id',
        '',
        InputOption::VALUE_REQUIRED,
        $this->trans('commands.generate.command_query_separation.command_handler.options.command_id')
      );

  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);

    $module = $input->getOption('module');
    $command_id = $input->getOption('command-id');
    $class_name = $input->getOption('class');
    $label = $input->getOption('label');
    $plugin_id = $input->getOption('plugin-id');

    $this->handlerGenerator->generate($module, $class_name, $label, $plugin_id, $command_id);


    $io->info($this->trans('commands.generate.command_query_separation.command_handler.messages.success'));

    $this->chainQueue->addCommand('cache:rebuild', ['cache' => 'discovery']);
  }

  protected function interact(InputInterface $input, OutputInterface $output)
  {
    $io = new DrupalStyle($input, $output);

    // --module option
    $module = $input->getOption('module');
    if (!$module) {
      // @see Drupal\Console\Command\Shared\ModuleTrait::moduleQuestion
      $module = $this->moduleQuestion($io);
      $input->setOption('module', $module);
    }

    // --class option
    $class = $input->getOption('class');
    if (!$class) {
      $class = $io->ask(
        $this->trans('commands.generate.command_query_separation.command_handler.questions.class'),
        'ExampleCommandHandler'
      );
      $input->setOption('class', $class);
    }

    // --plugin label option
    $label = $input->getOption('label');
    if (!$label) {
      $label = $io->ask(
        $this->trans('commands.generate.command_query_separation.command_handler.questions.label'),
        $this->stringConverter->camelCaseToHuman($class)
      );
      $input->setOption('label', $label);
    }

    // --name option
    $plugin_id = $input->getOption('plugin-id');
    if (!$plugin_id) {
      $plugin_id = $io->ask(
        $this->trans('commands.generate.command_query_separation.command_handler.questions.plugin_id'),
        $this->stringConverter->camelCaseToUnderscore($class)
      );
      $input->setOption('plugin-id', $plugin_id);
    }

    $command_id = $input->getOption('command-id');
    if (!$command_id) {
      // Gather valid command types.
      $command_options = [];
      foreach ($this->command_manager->getDefinitions() as $command) {
          $command_options[] = $command['id'];
      }

      $command_id = $io->choice(
        $this->trans('commands.generate.command_query_separation.command_handler.questions.command_id'),
        $command_options
      );

      $input->setOption('command-id', $command_id);
    }
  }
}
