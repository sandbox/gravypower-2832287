<?php

namespace Drupal\command_query_separation\Console\Command;

use Drupal\command_query_separation\Console\Generator\HandlerGenerator;
use Drupal\command_query_separation\PluginManagers\QueryManager;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\Console\Core\Utils\ChainQueue;
use Drupal\Console\Core\Utils\StringConverter;
use Drupal\Console\Extension\Manager;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Class GenerateCommandHandlerCommand.
 *
 * @package Drupal\command_query_separation
 */
class GenerateQueryHandlerCommand extends GenerateHandler {
  /**
   * @var \Drupal\command_query_separation\PluginManagers\QueryManager
   */
  private $query_manager;

  /**
   * PluginImageFormatterCommand constructor.
   * @param \Drupal\Console\Extension\Manager $extensionManager
   * @param \Drupal\command_query_separation\Console\Generator\HandlerGenerator $handlerGenerator
   * @param \Drupal\Console\Core\Utils\StringConverter $stringConverter
   * @param \Drupal\command_query_separation\PluginManagers\QueryManager $query_manager
   * @param \Drupal\Console\Core\Utils\ChainQueue $chainQueue
   */
  public function __construct(
    Manager $extensionManager,
    HandlerGenerator $handlerGenerator,
    StringConverter $stringConverter,
    QueryManager $query_manager,
    ChainQueue $chainQueue
  ) {

    parent::__construct($extensionManager, $stringConverter, $chainQueue, $handlerGenerator);

    $this->query_manager = $query_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('generate:command_query_separation:query_handler')
      ->setDescription($this->trans('commands.generate.command_query_separation.query_handler.description'))
      ->addOption('module', '', InputOption::VALUE_REQUIRED, $this->trans('commands.common.options.module'))
      ->addOption(
        'class',
        '',
        InputOption::VALUE_REQUIRED,
        $this->trans('commands.generate.command_query_separation.query_handler.options.class')
      )
      ->addOption(
        'label',
        '',
        InputOption::VALUE_OPTIONAL,
        $this->trans('commands.generate.command_query_separation.query_handler.options.label')
      )
      ->addOption(
        'plugin-id',
        '',
        InputOption::VALUE_OPTIONAL,
        $this->trans('commands.generate.command_query_separation.query_handler.options.plugin_id')
      )
      ->addOption(
        'query-id',
        '',
        InputOption::VALUE_REQUIRED,
        $this->trans('commands.generate.command_query_separation.query_handler.options.command_id')
      );

  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);

    $module = $input->getOption('module');
    $query_id = $input->getOption('query-id');
    $class_name = $input->getOption('class');
    $label = $input->getOption('label');
    $plugin_id = $input->getOption('plugin-id');

    $this->handlerGenerator->generate($module, $class_name, $label, $plugin_id, $query_id);


    $io->info($this->trans('commands.generate.command_query_separation.query_handler.messages.success'));

    $this->chainQueue->addCommand('cache:rebuild', ['cache' => 'discovery']);
  }

  protected function interact(InputInterface $input, OutputInterface $output)
  {
    $io = new DrupalStyle($input, $output);

    // --module option
    $module = $input->getOption('module');
    if (!$module) {
      // @see Drupal\Console\Command\Shared\ModuleTrait::moduleQuestion
      $module = $this->moduleQuestion($io);
      $input->setOption('module', $module);
    }

    // --class option
    $class = $input->getOption('class');
    if (!$class) {
      $class = $io->ask(
        $this->trans('commands.generate.command_query_separation.query_handler.questions.class'),
        'ExampleQueryHandler'
      );
      $input->setOption('class', $class);
    }

    // --plugin label option
    $label = $input->getOption('label');
    if (!$label) {
      $label = $io->ask(
        $this->trans('commands.generate.command_query_separation.query_handler.questions.label'),
        $this->stringConverter->camelCaseToHuman($class)
      );
      $input->setOption('label', $label);
    }

    // --name option
    $plugin_id = $input->getOption('plugin-id');
    if (!$plugin_id) {
      $plugin_id = $io->ask(
        $this->trans('commands.generate.command_query_separation.query_handler.questions.plugin_id'),
        $this->stringConverter->camelCaseToUnderscore($class)
      );
      $input->setOption('plugin-id', $plugin_id);
    }

    $query_id = $input->getOption('query-id');
    if (!$query_id) {
      // Gather valid command types.
      $query_options = [];
      foreach ($this->query_manager->getDefinitions() as $query) {
        $query_options[] = $query['id'];
      }

      $query_id = $io->choice(
        $this->trans('commands.generate.command_query_separation.query_handler.questions.command_id'),
        $query_options
      );

      $input->setOption('query-id', $query_id);
    }
  }
}
