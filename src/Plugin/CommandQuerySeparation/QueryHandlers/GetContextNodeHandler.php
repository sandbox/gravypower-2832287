<?php

namespace Drupal\command_query_separation\Plugin\CommandQuerySeparation\QueryHandlers;

use Drupal\command_query_separation\Plugin\CommandQuerySeparation\Queries\GetContextNode;
use Drupal\command_query_separation\QueryHandler;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @QueryHandler(
 *   id = "get_context_node_handler",
 *   title = @Translation("gets context node"),
 *   description = @Translation("Returns the context node"),
 *   query_id = "get_context_node"
 * )
 */
class GetContextNodeHandler extends QueryHandler implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private $route_match;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->route_match = $route_match;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match')
    );
  }

  /**
   * @param GetContextNode $query
   * @return mixed
   */
  function handle($query) {

    $node = $this->route_match->getParameter('node');

    if (gettype($node) == 'string') {
      $node = Node::load($node);
    }
    return $node;
  }
}