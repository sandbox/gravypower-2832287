<?php
/**
 * Created by PhpStorm.
 * User: aaron
 * Date: 1/10/17
 * Time: 3:48 PM
 */

namespace Drupal\command_query_separation\Plugin\CommandQuerySeparation\QueryHandlers;
use Drupal\command_query_separation\Plugin\CommandQuerySeparation\Queries\GetTaxonomyParents;
use Drupal\command_query_separation\QueryHandler;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\taxonomy\TermStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * @QueryHandler(
 *   id = "get_taxonomy_parents_handler",
 *   title = @Translation("gets context node"),
 *   description = @Translation("Returns the context node"),
 *   query_id = "get_taxonomy_parents"
 * )
 */
class GetTaxonomyParentsHandler extends QueryHandler implements ContainerFactoryPluginInterface {
  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @param GetTaxonomyParents $query
   * @return mixed
   */
  function handle($query) {
    /** @var TermStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('taxonomy_term');
    return $storage->loadParents($query->term->id());;
  }

  /**
   * GetTaxonomyParentsHandler constructor.
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entityTypeManager;
  }
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }
}