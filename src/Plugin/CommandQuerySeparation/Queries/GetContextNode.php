<?php
namespace Drupal\command_query_separation\Plugin\CommandQuerySeparation\Queries;


use Drupal\command_query_separation\IQuery;

/**
 * @Query(
 *   id = "get_context_node",
 *   title = @Translation("query for the context node"),
 *   description = @Translation("Query for the context node"),
 * )
 */
class GetContextNode implements IQuery {
  const id = "get_context_node";
}