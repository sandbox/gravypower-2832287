<?php

namespace Drupal\command_query_separation\Plugin\CommandQuerySeparation\Queries;

use Drupal\command_query_separation\IQuery;
use Drupal\taxonomy\Entity\Term;

/**
 * @property Term term
 * @Query(
 *   id = "get_taxonomy_parents",
 *   title = @Translation("query for the context node"),
 *   description = @Translation("Query for the context node"),
 * )
 */
class GetTaxonomyParents implements IQuery {

  /**
   * GetTaxonomyParents constructor.
   * @param Term $term
   */
  function __construct(Term $term) {
    $this->term = $term;
  }
}