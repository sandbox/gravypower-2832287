<?php

namespace Drupal\command_query_separation;


abstract class QueryHandler extends Handler implements IQueryHandler {
  /**
   * @return QueryHandler
   */
  public static function queryManager() {
    return \Drupal::getContainer()->get('plugin.manager.query_handlers');
  }
}