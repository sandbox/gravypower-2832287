<?php

namespace Drupal\command_query_separation\Exceptions;

use Exception;
use ReflectionClass;

class NoHandlerFound extends Exception{

  const message = "Handler for %s has not been registered";
  public function __construct($commandQuery)
  {
    if(gettype($commandQuery) != 'string')
    {
      $reflectionClass = new ReflectionClass($commandQuery);

      $commandQuery = $reflectionClass->getName();
    }
    parent::__construct(sprintf(NoHandlerFound::message, $commandQuery));
  }
}