<?php

namespace Drupal\command_query_separation\Exceptions;

use Drupal\command_query_separation\Handler;
use Exception;

class MultipleHandlersFound extends Exception{

  const message = "Multiple handlers for %s. \n %s \n";

  /**
   * MultipleHandlersFound constructor.
   * @param string $commandQuery
   * @param array $instances
   */
  public function __construct($commandQuery, $instances)
  {
    $handlers= array();
    /** @var Handler $instance */
    foreach ($instances as $instance)
    {
      $pluginDefinition = $instance->getPluginDefinition();
      $handlers[] = $instance->getPluginId() . ' - ' . $pluginDefinition['class'];
    }

    parent::__construct(sprintf(MultipleHandlersFound::message, $commandQuery, implode("\n", $handlers)));
  }
}